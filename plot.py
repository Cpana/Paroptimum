import numpy as np

from PyQt5.QtWidgets import QDialog, QErrorMessage
from sympy import symbols, lambdify
import matplotlib.pyplot as plt
from matplotlib.widgets import Slider


class MplWindow(QDialog):

    def __init__(self, parent=None):
        super(MplWindow, self).__init__(parent)

    def plot(self, expr, paramList, xmin, xmax):

            'Splitten des Funktionen-Strings in die einzelnen "aeußeren" Funktionen'
            functions = expr.split(",")

            '''Der Definitionsbereich, wird einmal fuer alle Plots in der GUI festgelegt.'''
            stepsize = round((float(xmax)-float(xmin))/200, 4)
            x_vals = np.arange(float(xmin), float(xmax), stepsize)

            'Interaktiver Modus zeichnet Graphen nach Veränderung neu'
            plt.ion()
            '''Abfrage ob die parameterListe und die lange der functions > 0 sind --> wenn es nur eine Variable ist wird die Horizontale und Vertiakle Axe mit
               0 und 0 initialisiert und es wird keinen Fehler und der Graph wird nicht geplottet'''

            if len(paramList) <= 1 and len(functions) <= 1:
                fig, axs = plt.subplots(1,1, squeeze=False)
            else:
                fig, axs = plt.subplots(len(paramList), len(functions), squeeze=False)

            sliderliste = []
            'plt.axes([0.1, 0.2, 0.8, 0.05])'

            axSlider = []
            botscale = 1
            for paramString in paramList:
                paramdata = paramString.split(",")
                '''Differenz (Max-Min)/n => n Schritte'''
                stepsize = round((int(paramdata[2])-int(paramdata[1]))/100, 4)
                axtemp = plt.axes([0.1, 0.05*botscale, 0.8, 0.02])
                axSlider.append(axtemp)
                slider = Slider(axSlider.pop(), paramdata[0], valmin=int(paramdata[1]), valmax=int(paramdata[2]), valstep=stepsize)
                slider.set_val(round(((int(paramdata[2])+int(paramdata[1]))/2), 4))
                sliderliste.append(slider)
                botscale = botscale + 0.8
                plt.subplots_adjust(bottom=0.4, hspace=0.4, top=0.8)

            plt.show()

            def updateFunctions(val):

                'Aeußere Funktionen werden horizontal gestreut'
                horizAnzeige = 0
                'Innere Funktionen werden vertikal gestreut'
                vertAnzeige = 0
                for f in functions:

                    'Symbole rausfiltern aus der Equation und eine Liste für die Iteration zu erzeugen'
                    paramPerFunction = aSymbols(f)

                    'Fuer jede "innere Funktion"(Querschnitt nach jeweils einer Variable einer "aeußeren Funktion"'
                    for p in paramPerFunction:

                        symboleTemp = [el for el in paramPerFunction if el not in p]
                        fn = f
                        for i in range(len(symboleTemp)):
                            for slider in sliderliste:
                                paramdata = slider.label.get_text()
                                if paramdata == symboleTemp[i]:
                                    newparamvalue = slider.val

                            fn = fn.replace(symboleTemp[i], str(round(newparamvalue, 4)))
                        psymbol = symbols(p)
                        'Für Dividierungen durch Null'
                        nan = False
                        fn = fn.lower()
                        try:
                            lam = lambdify(psymbol, fn, "numpy")
                            y_vals = lam(x_vals)
                        except KeyError:
                            nan = True
                        except Exception:
                            plt.close('all')
                            error_dialog = QErrorMessage()
                            error_dialog.showMessage('Fehler: Die Funktion konnte nicht interpretiert werden.')
                            error_dialog.exec_()
                            return -1

                        'Wurde für y_val nan errechnet'
                        if not nan and not isinstance(y_vals, np.ndarray) and np.isnan(y_vals):
                            nan = True
                            'Wurde für y_val ein konstanter Wert errechnet'
                        elif not nan and not isinstance(y_vals, np.ndarray):
                            y_vals = (x_vals/x_vals)*y_vals

                        if len(functions) == 1 and not nan:

                            axs[vertAnzeige,horizAnzeige].clear()
                            axs[vertAnzeige, horizAnzeige].set_title(f + " nach " + p )
                            axs[vertAnzeige, horizAnzeige].plot(x_vals, y_vals)
                            axs[vertAnzeige,horizAnzeige].grid()

                        elif len(functions) > 1 and not nan:
                            axs[vertAnzeige, horizAnzeige].clear()
                            axs[vertAnzeige, horizAnzeige].set_title(f + " nach " + p)
                            axs[vertAnzeige, horizAnzeige].plot(x_vals, y_vals)
                            axs[vertAnzeige, horizAnzeige].grid()
                        elif not nan:
                            if len(functions) == 1:
                                axs[vertAnzeige].clear()
                            elif len(functions) > 1:
                                axs[vertAnzeige, horizAnzeige].clear()
                        fig.canvas.flush_events()
                        vertAnzeige = vertAnzeige + 1
                    'Die zweite Funktion wird nach rechts gestreut'
                    horizAnzeige = horizAnzeige + 1
                    vertAnzeige = 0

            for slider in sliderliste:
                slider.on_changed(updateFunctions)
            val = 0
            updateFunctions(val)


def aSymbols(eq):
    listeAllerSymbole = []
    resultSet = []
    for i in eq:
        if i.islower():
            listeAllerSymbole.append(i)

    for i in listeAllerSymbole:
        if i not in resultSet:
            resultSet.append(i)

    return resultSet




