import unittest


def getTextFunktion(functString):
    functionString = functString
    paramList = []
    for c in functionString:
        if c.isalpha() and (c not in paramList):
            paramList.append(c)
    if not paramList: return -1
    return paramList


class testgetTextFunKtion(unittest.TestCase):
    def testgetTextfunction1(self):
        self.assertEqual(getTextFunktion(""), -1)
    def testgetTextfunction2(self):
        self.assertEqual(getTextFunktion("abs"), ['a', 'b', 's'])
    def testgetTextfunction3(self):
        self.assertEqual(getTextFunktion("a^2-1*b/54*s"), ['a', 'b', 's'])
    def testgetTextfunction4(self):
        self.assertEqual(getTextFunktion("s*3/42-gs"), ['s', 'g'])
    def testgetTextfunction5(self):
        self.assertEqual(getTextFunktion("aaaaaaaaaaaaaa"), ['a'])
    def testgetTextfunction6(self):
        self.assertEqual(getTextFunktion("24*56/234-4567"), -1)


if __name__ == '__main__':
    unittest.main()
