import sys

from PyQt5.QtWidgets import QPushButton, QHBoxLayout, QVBoxLayout, QWidget, QLabel, QLineEdit, QMessageBox, QErrorMessage

import plot


class UiMainWindow(QWidget):

    def __init__(self):
        super().__init__()

        self.hboxParamList = []

        self.okButton = QPushButton("Plot")
        self.okButton.setHidden(True)
        self.cancelButton = QPushButton("Beenden")
        self.insertButton = QPushButton("Einfügen")
        self.functionLabel = QLabel('Funktion', self)
        self.functionEdit = QLineEdit()
        self.definitionLabel = QLabel('Definitionsbereich: ', self)
        self.definitionMinLabel = QLabel('Min:', self)
        self.definitionMinEdit = QLineEdit()
        self.definitionMaxLabel = QLabel('Max:', self)
        self.definitionMaxEdit = QLineEdit()
        self.paramTitleLabel = QLabel('Parameterkonfiguration:', self)
        self.paramTitleLabel.setHidden(True)

        self.vbox = QVBoxLayout()
        self.hboxParamTitle = QHBoxLayout()
        self.hboxParamTitle.addWidget(self.paramTitleLabel)
        self.hboxParamTitle.addStretch(1)

        self.initUI()

    def initUI(self):

        self.setGeometry(300, 300, 300, 200)
        self.setWindowTitle('Paroptimum Version 1.1')

        '''Button Actions'''
        self.cancelButton.clicked.connect(self.buttonClicked)
        self.insertButton.clicked.connect(self.buttonClicked)
        self.okButton.clicked.connect(self.buttonClicked)

        hboxPlotExit = QHBoxLayout()
        hboxPlotExit.addWidget(self.okButton)
        hboxPlotExit.addStretch(1)
        hboxPlotExit.addWidget(self.cancelButton)

        hboxFunction = QHBoxLayout()
        hboxFunction.addWidget(self.functionLabel)
        hboxFunction.addWidget(self.functionEdit)
        hboxFunction.addWidget(self.insertButton)
        hboxFunction.addStretch(1)

        hboxDefValues = QHBoxLayout()
        hboxDefValues.addWidget(self.definitionLabel)
        hboxDefValues.addWidget(self.definitionMinLabel)
        hboxDefValues.addWidget(self.definitionMinEdit)
        hboxDefValues.addWidget(self.definitionMaxLabel)
        hboxDefValues.addWidget(self.definitionMaxEdit)
        hboxDefValues.addStretch(1)

        self.vbox.addLayout(hboxFunction)
        self.vbox.addLayout(hboxDefValues)
        self.vbox.addLayout(hboxPlotExit)
        self.vbox.addLayout(self.hboxParamTitle)

        self.setLayout(self.vbox)

        self.show()

    def updateUI(self):

        self.paramTitleLabel.setHidden(True)
        self.okButton.setHidden(True)
        hboxParamDelList = []
        for hbox in self.hboxParamList:
            delete = True
            for p in self.parList:
                if hbox.itemAt(0).widget().text() == p:
                    delete = False
            if delete:
                for i in reversed(range(hbox.count())):
                    hbox.itemAt(i).widget().setParent(None)
                hboxParamDelList.append(hbox)
                self.removeLayoutFromBox(self.vbox, hbox)
        'Man kann nicht innerhalb einer Schleife einer Liste die Elemente löschen, daher die Hilfsliste hboxdel'
        for hboxdel in hboxParamDelList:
            self.hboxParamList.remove(hboxdel)

        i = 1
        for p in self.parList:

            if i == 1:
                self.paramTitleLabel.setHidden(False)
                self.okButton.setHidden(False)

            add = True
            for hbox in self.hboxParamList:
                if hbox.itemAt(0).widget().text() == p:
                    add = False
            if add:
                hboxtemp = QHBoxLayout()
                hboxtemp.addWidget(QLabel(p))
                hboxtemp.addWidget(QLabel('->  Min:'))
                hboxtemp.addWidget(QLineEdit())
                hboxtemp.addWidget(QLabel('Max:'))
                hboxtemp.addWidget(QLineEdit())

                self.hboxParamList.append(hboxtemp)
                self.vbox.addLayout(self.hboxParamList[len(self.hboxParamList) - 1])

            i = i + 1

    def removeLayoutFromBox(self, containerBox, deleteObject):
        for i in range(containerBox.count()):
            layout_item = containerBox.itemAt(i)
            if layout_item.layout() == deleteObject:
                containerBox.removeItem(layout_item)
                return

    def buttonClicked(self):
        sender = self.sender()
        if sender.text() == "Beenden":
            sys.exit()
        elif sender.text() == "Einfügen":
            if self.getTextFunktion() == -1:
                error_dialog = QErrorMessage()
                error_dialog.showMessage('Fehler: Die Funktion wurde falsch eingegeben.')
                error_dialog.exec_()
                return
            self.parList = self.getTextFunktion()
            self.updateUI()
        elif sender.text() == "Plot":
            err = self.opengraph()
            if isinstance(err, int):
                error_dialog = QErrorMessage()
                if err == -1:
                    error_dialog.showMessage('Fehler: Der Definitionsbereich wurde nicht richtig gesetzt.')
                elif err == -2:
                    error_dialog.showMessage('Fehler: Mindestens ein Parameter wurde nicht richtig konfiguriert.')
                elif err == -3:
                    error_dialog.showMessage('Fehler: Ein Parameter kann immer nur ein Buchstabe sein. '
                                             'Multiplikationen explizit mit *.')
                elif err == -4:
                    error_dialog.showMessage('Fehler: Ein Parameter wurde noch nicht mit Einfügen hinzugefügt.')
                elif err == -5:
                    error_dialog.showMessage('Fehler: Der Funktionsausdruck konnte nicht ausgewertet werden.')
                error_dialog.exec_()

    def opengraph(self):
        definMin = self.definitionMinEdit.text()
        definMax = self.definitionMaxEdit.text()
        'Schutz vor falschen Eingaben:'
        if definMin == '' or definMax == '': return -1
        try:
            hilf = definMin
            float(hilf)
            hilf = definMax
            float(hilf)
        except ValueError:
            return -1
        functionString = self.functionEdit.text()
        paramList = self.getParamValueList()
        if paramList == -1: return -2
        if not self.checkFunctionString(functionString): return -3
        if len(self.getTextFunktion()) != len(paramList): return -4
        plot.MplWindow.plot(self, functionString, paramList, definMin, definMax)  # +++

    def close_application(self):
        choice = QMessageBox.question(self, 'Message',
                                      "Möchten Sie wirklich beenden?", QMessageBox.Yes |
                                      QMessageBox.No, QMessageBox.No)
        if choice == QMessageBox.Yes:
            sys.exit()
        else:
            pass

    def getTextFunktion(self):
        functionString = self.functionEdit.text()
        paramList = []
        for c in functionString:
            if c.islower() and (c not in paramList):
                paramList.append(c)
        if not paramList: return -1
        return paramList

    def getParamValueList(self):
        parRetList = []

        for hbox in self.hboxParamList:
            paramname = hbox.itemAt(0).widget().text()
            parammin = hbox.itemAt(2).widget().text()
            parammax = hbox.itemAt(4).widget().text()
            if paramname == '' or parammin == '' or parammax == '':
                return -1
            try:
                hilfmin = parammin
                float(hilfmin)
                hilfmax = parammax
                float(hilfmax)
            except ValueError:
                return -1
            if hilfmin >= hilfmax:
                return -1
            parRetList.append(paramname + "," + parammin + "," + parammax)
        return parRetList

    def checkFunctionString(self, functionString):
        einParam = 0
        for c in functionString:
            if c.islower():
                einParam = einParam + 1
            else:
                einParam = 0
            if einParam > 1:
                return False
        return True
