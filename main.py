import sys

'Die import dient nur dazu die Libs mit Pyinstaller zu verbinden sodas sich pyinstaller eine ausführbare Exe erstellt '
import PyQt5
import matplotlib.pyplot
import sympy
import numpy


from PyQt5.QtWidgets import QApplication

from ui_main import UiMainWindow


def main():
    app = QApplication(sys.argv)  #
    gui = UiMainWindow()
    app.exec_()


main()
